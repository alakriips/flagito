/* AIRPORTS LIST AT FRONT PAGE -- CAN DELETE THAT */
var options = {
  shouldSort: true,
  threshold: 0.4,
  maxPatternLength: 32,
  keys: [{
    name: 'iata',
    weight: 0.5
  }, {
    name: 'name',
    weight: 0.3
  }, {
    name: 'city',
    weight: 0.2
  }]
};

var fuse = new Fuse(airports, options)


var ac = $('#autocomplete')
  .on('click', function (e) {
    e.stopPropagation();
  })
  .on('focus keyup', search)
  .on('keydown', onKeyDown);

var wrap = $('<div>')
  .addClass('autocomplete-wrapper')
  .insertBefore(ac)
  .append(ac);

var list = $('<div>')
  .addClass('autocomplete-results')
  .on('click', '.autocomplete-result', function (e) {
    e.preventDefault();
    e.stopPropagation();
    selectIndex($(this).data('index'));
  })
  .appendTo(wrap);

$(document)
  .on('mouseover', '.autocomplete-result', function (e) {
    var index = parseInt($(this).data('index'), 10);
    if (!isNaN(index)) {
      list.attr('data-highlight', index);
    }
  })
  .on('click', clearResults);

function clearResults() {
  results = [];
  numResults = 0;
  list.empty();
}

function selectIndex(index) {
  if (results.length >= index + 1) {
    ac.val(results[index].iata);
    clearResults();
  }
}

var results = [];
var numResults = 0;
var selectedIndex = -1;

function search(e) {
  if (e.which === 38 || e.which === 13 || e.which === 40) {
    return;
  }

  if (ac.val().length > 0) {
    results = _.take(fuse.search(ac.val()), 7);
    numResults = results.length;

    var divs = results.map(function (r, i) {
      return '<div class="autocomplete-result" data-index="' + i + '">'
        + '<div><b>' + r.iata + '</b> - ' + r.name + '</div>'
        + '<div class="autocomplete-location">' + r.city + ', ' + r.country + '</div>'
        + '</div>';
    });

    selectedIndex = -1;
    list.html(divs.join(''))
      .attr('data-highlight', selectedIndex);

  } else {
    numResults = 0;
    list.empty();
  }
}

function onKeyDown(e) {
  switch (e.which) {
    case 38: // up
      selectedIndex--;
      if (selectedIndex <= -1) {
        selectedIndex = -1;
      }
      list.attr('data-highlight', selectedIndex);
      break;
    case 13: // enter
      selectIndex(selectedIndex);
      break;
    case 9: // enter
      selectIndex(selectedIndex);
      e.stopPropagation();
      return;
    case 40: // down
      selectedIndex++;
      if (selectedIndex >= numResults) {
        selectedIndex = numResults - 1;
      }
      list.attr('data-highlight', selectedIndex);
      break;

    default: return; // exit this handler for other keys
  }
  e.stopPropagation();
  e.preventDefault(); // prevent the default action (scroll / move caret)
}

/* MOBILE MENU */
$(".mobileMenuToggle").click(function () {
  $(this).toggleClass('toggleVisible').toggleClass('toggleHidden');
  $('.navbar-collapse').toggleClass('isMobile');
  $(".navbar-collapse").fadeToggle();
});

/* CLIENT SLIDER */

$(document).ready(function () {
  if ($('.clients__slider').length) {
    $('.clients__slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1500,
      arrows: false,
      dots: false,
      pauseOnHover: false,
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 3
        }
      }, {
        breakpoint: 520,
        settings: {
          slidesToShow: 2
        }
      }]
    });
  }
});

/* FORM ADD CONNECTING FLIGHT */
$(document).ready(function () {
  var counter = 0;
  $("#connectingflight__button--add").click(function () {
    counter++;
    $(".connectingflight").append("<div class='form-group position-relative'><label>Connecting flight #" + counter + "</label><input type='text' class='form-control form-control-lg'><a href='javascript:;' class='connectingflight__button--remove'><img src='img/form-remove.svg' class='form__icon form__icon--remove'></a><img src='img/connecting-flight.svg' class='connectingflight__icon pt-2'></div>").hide().fadeIn(1000);

  });
});

/* FORM REMOVE CONNECTING FLIGHT */
$(".connectingflight").on('click', '.connectingflight__button--remove', function () {
  $(this).parent().fadeOut(500, function () { $(this).remove(); });
});

/* BOOKING TOOLTIP */
$(".form__icon--booking").hover(function () {
  $(".form__details__tooltip").fadeIn(500);
}, function () {
  $(".form__details__tooltip").fadeOut(500);
});

/* BOOKING DATEPICKER */
$('.datepicker').datepicker({
  clearBtn: true,
  format: "dd/mm/yyyy",
  autoclose: true
});
$('#reservationDate').on('change', function () {
  var pickedDate = $('input').val();
  $('#pickedDate').html(pickedDate);
});
$('#reservationClick').click(function () {
  $('#reservationDate').focus();
})

/* SHOW DATE OF BIRTH INSTEAD OF ID NUMBER */
if ($('#idCheck').is(':checked')) {
  $("#personaldetails__id").prop("disabled", true);
  $("#personaldetails__datebirth").fadeIn(500);
}
$('#idCheck').click(function () {
  if ($('#idCheck').is(':checked')) {
    $("#personaldetails__id").prop("disabled", true);
    $("#personaldetails__datebirth").fadeIn(500);
  } else {
    $("#personaldetails__id").prop("disabled", false);
    $("#personaldetails__datebirth").fadeOut(500);
  }
});

/* PERSONAL DETAILS DATEPICKER */
$('.datepicker').datepicker({
  clearBtn: true,
  format: "dd/mm/yyyy",
  autoclose: true
});
$('#detailsDate').on('change', function () {
  var pickedDate = $('input').val();
  $('#pickedDate').html(pickedDate);
});
$('#detailsClick').click(function () {
  $('#detailsDate').focus();
})

/* ADDITIONAL PASSENGERS */
$('#addPassenger').click(function () {
  $('.form__addpassangers--choice').fadeToggle(500);
})
$(document).ready(function () {
  var passengerCounter = 0;
  $('#addAdult').click(function () {
    passengerCounter++;
    $('.form__extrapassenger--inner').append("<div class='col-lg-8 col-md-8 offset-lg-2 offset-md-2 pt-4'><label class='pb-3'>Additional passenger #" + passengerCounter + "</label><div class='row'><div class='form-group col-md-6'><label>Name</label><input type='text' class='form-control form-control-lg' placeholder=''></div><div class='form-group col-md-6'><label>E-mail</label><input type='email' class='form-control form-control-lg' placeholder=''><img src='img/form-remove.svg' class='form__icon form__icon--removePassenger d-none d-sm-none d-md-none d-lg-block'></div><div class='col-sm-12 removepassenger__button d-block d-sm-block d-md-block d-lg-none'><a class='form__button--blog form__button--signature form__button--removePassenger' href='javascript:;'>Remove passenger #" + passengerCounter + "</a></div></div>").hide().fadeIn(1000);;
  })
  $('#addChild').click(function () {
    passengerCounter++;
    $('.form__extrapassenger--inner').append("<div class='col-lg-8 col-md-8 offset-lg-2 offset-md-2 pt-4'><label class='pb-3'>Additional passenger #" + passengerCounter + "</label><div class='row'><div class='form-group col-md-4'><label>Name</label><input type='text' class='form-control form-control-lg' placeholder=''></div><div class='form-group position-relative col-md-4'><label>Date of birth</label><div class='datepicker date input-group p-0'><input type='text' placeholder='' class='form-control py-4 px-4'><div class='input-group-append'></div></div></div><div class='form-group position-relative col-md-4'><ul class='form__distruption__contact'><label>Are you the parent?</label><div class='d-flex justify-content-center childpassenger__selector'><li><input type='radio' id='contact1-option' name='selector-parent' checked><label for='contact1-option'>Yes</label><div class='distruption--check'></div></li><li><input type='radio' id='contact2-option' name='selector-parent'><label for='contact2-option'>No</label><div class='distruption--check'></div></li><img src='img/form-remove.svg' class='form__icon form__icon--removeChild d-none d-sm-none d-md-none d-lg-block'></div></ul></div><div class='col-sm-12 removechild__button d-block d-sm-block d-md-block d-lg-none'><a class='form__button--blog form__button--signature form__button--removeChild' href='javascript:;'>Remove passenger #" + passengerCounter + "</a></div></div></div>").hide().fadeIn(1000);
  })
  $(".form__extrapassenger--inner").on('click', '.form__icon--removePassenger', function () {
    $(this).parent().parent().parent().fadeOut(500, function () { $(this).remove(); });
    passengerCounter--;
  });
  $(".form__extrapassenger--inner").on('click', '.form__button--removePassenger', function () {
    $(this).parent().parent().parent().fadeOut(500, function () { $(this).remove(); });
    passengerCounter--;
  });
  $(".form__extrapassenger--inner").on('click', '.form__icon--removeChild', function () {
    $(this).parent().parent().parent().parent().parent().fadeOut(500, function () { $(this).remove(); });
    passengerCounter--;
  });
  $(".form__extrapassenger--inner").on('click', '.form__button--removeChild', function () {
    $(this).parent().parent().parent().fadeOut(500, function () { $(this).remove(); });
    passengerCounter--;
  });

});


$('.datepicker').datepicker({
  clearBtn: true,
  format: "dd/mm/yyyy",
  autoclose: true
});
$('#detailsDate').on('change', function () {
  var pickedDate = $('input').val();
  $('#pickedDate').html(pickedDate);
});
$('#detailsClick').click(function () {
  $('#detailsDate').focus();
});


/* SIGNATURE */
$(function () {
  if ($('#signature__canvas').length) {
    $('#signature__canvas').signature({
      color: 'black',
      background: '#f7f7f7'
    });
    $('#clear').click(function () {
      $('#signature__canvas').signature('clear');
    });
  }
});

/* DATA TAB FIX FOR DISTRUPTION DETAILS */
$('input[name="selector"]').click(function () {
  $(this).tab('show');
  $(this).removeClass('active');
});


/* MULTISTEP NUMBER */
// $(function () {
//   if ($('.multisteps-form__progress-btn').length) {
//     if ($(window).width() < 500) {
//       var counter = 1;
//       $('.multisteps-form__progress-btn').each(function () {
//         $(this).html(counter);
//         counter++;
//       });
//     }
//   }
// });

$(function () {
  if ($('.multisteps-form__progress-btn').length) {
    if ($(window).width() < 500) {
      var counter = 1;
      $('.multisteps-form__progress-btn').each(function () {
        $(this).html('');
        counter++;
      });
    }
  }
});

